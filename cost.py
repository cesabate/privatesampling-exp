import math as m
import numpy as np 
import matplotlib.pyplot as plt
import classicsigmap as clsp

plt.rcParams.update({'font.size': 16}) # font size

def rangep(k): 
    """ 
    Computes the inputs and number of multiplications of 
    a circuit for range [0, 2^k) proof 
    Parameters: 
    -----------
    k: exponent of the upper bound of the range
    Returns: a tuple (n, m, m0) 
    n: number of inputs
    m: number of multiplications
    m0: number of multiplication gates that have output 0
    """ 
    n = k + 1  # the extra input is the number in the range
    m = k
    m0 = k # number of multiplication gates that are 0
    return n, m, m0  
    
def grange(a,b):
    """ 
    Computes the inputs and number of multiplications of 
    a circuit for range [a,b] proof
    
    Parameters: 
    -----------
    a: lower bound of the range (public)
    b: upper bound of the range (public)
    
    Returns: 
    --------
    n: number of inputs
    m: number of multiplications
    m0: the number of multiplication gates that are 0 
    """ 
    k = m.floor(m.log2(b-a))+1 
    nr, mr, m0r = rangep(k)
    return 2*nr -1, 2*mr, 2*m0r
    
    
def bitshift(k):
    """ 
    Computes nbr of inputs and multiplications of a right bit shift proof
    of magnitude k 
    
    Parameters: 
    -----------
    k: magnitude of the righ bit shift
    
    Returns: 
    --------
    n: number of inputs
    m: number of integer multiplications
    m0: the number of multiplication gates that are 0 
    -------
    """ 
    nr, mr, m0r = rangep(k) 
    return nr +1, mr, m0r
    
def fracprod(st):
    """ 
    Computes the inputs and multiplications of a product proof
    with fractional storage st
    
    Parameters:
    -----------
    st: number of decimal bits on our representation
    
    Returns: 
    --------
    n: number of inputs
    m: number of integer multiplications
    m0: the number of multiplication gates that are 0 
    """
    
    nr, mr, m0r = rangep(st)
    return nr + 2, mr + 1, m0r

def fracdivp(bt): 
    """
    Computes the number of inputs and multiplications of a division proof, 
    where the divisor is in a public range [A,B] 
    with bt = m.floor(log2(B-A)) +1
    
    Parameters:
    -----------
    bt: bits of the range of the divisor
    
    Returns: 
    --------
    n: number of inputs
    m: number of integer multiplications
    m0: the number of multiplication gates that are 0 
    """
    nr, mr, m0r = rangep(bt)
    return 2*nr +1, 2*mr +1, 2*m0r 




def intexp(k):
    """
    Computes the number of inputs and multiplications of
    integer exponentiation proof y= E^x, where x 
    is an integer in [0, 2^k)
    
    Parameters:
    -----------
    
    Returns: 
    --------
    n: number of inputs
    m: number of integer multiplications
    """
    nr, mr, m0r = rangep(k)
    return nr+1, mr + k -1, m0r 
    

def modsum(k):
    """
    Computes the number of inputs and multiplications of
    modular sum proof z = x + y mod 2^k, 
    where integers x, z in [0, 2^k) are private, and y in [0, 2^k)
    is public 
    
    Parameters:
    -----------
    k: number of bits of the inputs and output
    
    Returns: 
    --------
    n: number of inputs
    m: number of integer multiplications between two private values
    m0: the number of multiplication gates that are 0 
    """
    nr, mr, m0r = rangep(k) 
    return 2*nr + 1, 2*mr +1, 2*m0r + 1
  
def shiftF(i, cmod): 
    """
    Computes the value of the shift magnitude F_i of cordic
    depending on the curve cmod parameter
   
    Parameters:
    -----------
    i: integer, current iteration
    cmod: in {-1, 1}, curve parameter
    """
    assert(cmod  == 1 or cmod == -1)
    if (cmod==1):
        return i 
    # else
    k=0
    while 3 ** (k+1) + 2*k + 1 <= 2 *i: 
        k+=1
    return i - k 
    
def cordic(nit, cmod):
    """
    Computes the number of inputs and multiplications of
    the core of cordic with n iterations
    
    Parameters:
    -----------
    n: number of iteratios of cordic
    cmod: mode of operation, in our case either 1 or -1
    
    Returns: 
    --------
    n: number of inputs
    m: number of integer multiplications between two private values
    m0: the number of multiplication gates that are 0 
    """
    nfin = 0 
    mfin = 0 
    m0fin = 0
    # count bit shifts
    for i in range(nit):
        ns, ms, m0s = bitshift(shiftF(i+1, cmod))
        nfin += 2 * ns
        mfin += 2 * ms
        m0fin += 2* m0s
    
    # direction inputs and domain proof
    nfin += nit
    mfin += nit 
    m0fin += nit
    
    # X_i = X_{i-1} + \xi ( X_{i-1} ) for all nit and similar for Y_i
    mfin += 2 * nit
    
    # add inputss X_n, Y_n and Theta_n
    nfin += 3
    return nfin, mfin, m0fin
    
def radixx(i, cmod): 
    """
    Computes the constant angle radix 
    values used for cordic at iteration i with with 
    curve paremeter equal to cmod
    
    Parameters:
    -----------
    i: int, number if iterations
    cmod: {-1, 1} curve parameter
    
    Returns:
    -------
    rad: radix angle 
    
    """
    assert(cmod == 1 or cmod == -1) 
    if (cmod ==1):
        return m.atan(2**(-i))
    # else 
    return m.atanh(2**(-i))
    
def cordic1(storage, nit): 
    """
    Computes the number of inputs and multiplications of
    the cordic Setting 1 with n iterations
    
    Parameters:
    -----------
    storage:  storage of our fractional representation
    nit: number of iterations of cordic
    
    Returns: 
    --------
    n: number of inputs
    m: number of integer multiplications between two private values
    m0: the number of multiplication gates that are 0 
    """
    assert(storage >= nit + m.ceil(m.log2(nit)))
    cmod = 1 # curve
    nc, mc, m0c = cordic(nit, cmod) 
    gamma = m.floor(m.log2(2 * radixx(nit, cmod))) + 1 + storage
    nr, mr, m0r = rangep(gamma) 
    
    return nc + nr, mc + mr, m0c + m0r
    
    
def cordic2(storage, nit): 
    """
    Computes the number of inputs and multiplications of
    the cordic mode 2 and curve = -1
    
    Parameters:
    -----------
    storage:  storage of our fractional representation
    nit: number of iterations of cordic
    
    Returns: 
    --------
    n: number of inputs
    m: number of integer multiplications between two private values
    m0: the number of multiplication gates that are 0 
    """
    assert(storage >= nit + m.ceil(m.log2(nit)))
    cmod = -1
    nc, mc, m0c = cordic(nit, cmod)
    nr, mr, m0r = rangep(storage - shiftF(nit, cmod))
    
    return nc + nr  -1, mc + mr, m0c + m0r
    
    
def ctrig(storage, nit):
    """
    Computes the number of inputs and multiplications of
    cordic parametrized for sine and cosine
    
    Parameters:
    -----------
    storage:  storage of our fractional representation
    nit: number of iterations of cordic
    
    Returns: 
    --------
    n: number of inputs
    m: number of integer multiplications between two private values
    m0: the number of multiplication gates that are 0 
    """
    nc, mc, m0c = cordic1(storage, nit) 
    return nc - 2, mc, m0c
    
def clog(storage, nit):
    """
    Computes the number of inputs and multiplications of
    cordic parametrized for natural logarithm
    
    Parameters:
    -----------
    storage:  storage of our fractional representation
    nit: number of iterations of cordic
    
    Returns: 
    --------
    n: number of inputs
    m: number of integer multiplications between two private values
    m0: the number of multiplication gates that are 0 
    """
    nc, mc, m0c = cordic2(storage, nit)
    return nc, mc, m0c 

def csqrt(storage, nit): 
    """
    Computes the number of inputs and multiplications of
    cordic parametrized for square root
    
    Parameters:
    -----------
    storage:  storage of our fractional representation
    nit: number of iterations of cordic
    m0: the number of multiplication gates that are 0 
    
    Returns: 
    --------
    n: number of inputs
    m: number of integer multiplications between two private values
    """
    nc, mc, m0c = cordic2(storage, nit)
    return nc - 1, mc, m0c
    
def ctrigGen(storage, nit): 
    """
    Computes the number of inputs and multiplications of
    the generalization to de domain [-pi, pi]  of sine and cosine
    using cordic 
    
    Parameters:
    -----------
    storage:  storage of our fractional representation
    nit: number of iterations of cordic
    
    Returns: 
    --------
    n: number of inputs
    m: number of integer multiplications between two private values
    m0: the number of multiplication gates that are 0 
    """

    nc, mc, m0c = ctrig(storage, nit)
    nr, mr, m0r = grange( - m.pi * (2 ** storage), m.pi * (2 ** storage) -1)
        
    mextra = 4 * 3 # extra multiplications due to modulus check
    m0extra = 4 * 3 # the above are all equal to 0
    nextra = 2 # extra inputs, -1 by theta', +3 theta, s, c
    
    return nc + nr + nextra, mc + mr + mextra, m0c + m0r + m0extra
    
def clogGen(storage, nit):
    """
    Computes the number of inputs and multiplications of
    the generalization to de domain (0, 1)  of logarithm 
    function using cordic
    
    Parameters:
    -----------
    storage:  storage of our fractional representation
    nit: number of iterations of cordic
    
    Returns: 
    --------
    n: number of inputs
    m: number of integer multiplications between two private values
    m0: the number of multiplication gates that are 0 
    """
    nc, mc, m0c = clog(storage, nit)
    nr, mr, m0r = rangep(storage-1)
    nie, mie, m0ie = intexp(storage-1)
    
    mextra = 1 # extra multiplication 2^e x = x' 
    nextra = 1 # -1 for range proof, +2 for x, l 
     
    return nc + nr + nie + nextra, mc + mr + mie + mextra, m0c + m0r + m0ie
    
    
def pbshift(kp):
    niexp, miexp, m0iexp = intexp(kp)
    nr, mr, m0r = rangep(kp) 
    
    nextr = -1 # -2 range proofs, +1 for x
    mextr = 1 #  extra mult of h * y
    
    return niexp + 2*nr + nextr, miexp + 2*mr + mextr,  m0iexp + 2*m0r 
    
    
    
def csqrtGen(storage, nit, ubound): 
    """
    Computes the number of inputs and multiplications of
    the generalization to de domain [0, 2 storage ln(2)) 
    of square root function using cordic
    
    Parameters:
    -----------
    storage:  storage of our fractional representation
    nit: number of iterations of cordic
    ubound: upper bound of the square root parameter of the input for the generalization of domain 
    
    Returns: 
    --------
    n: number of inputs
    m: number of integer multiplications between two private values
    m0: the number of multiplication gates that are 0 
    """ 
    nc, mc, m0c = csqrt(storage,nit)
    
    nr, mr, m0r = rangep(storage-1)
    
    gamma = m.floor(m.log2( ubound )) +1
    
    nps1, mps1, m0ps1 = pbshift(gamma)
    niex1, miex1, m0iex1 = intexp(gamma) 
    nps2, mps2, m0ps2 = pbshift(storage-1)
    niex2, miex2, m0iex2 = intexp(storage-1)

    mextra = 12 # +2 bits i_e(1 - i_e), s_e * (1 - s_e) # +4 first circuit, +4 second circuit
             # +2 for  s' * l and h * x 
    m0extra  =  2 # control bits

    nextra =  -4 # -1 range proof +1 e  -2 pshift1 -1 iex2  -1 pshift2
    
    
    ntot = nc + nr + nps1 + niex1 + nps2 + niex2 + nextra
    mtot = mc + mr +   (mps1 + miex1 + mps2 + miex2) + mextra #optimized  for conditionals 
    m0tot =  m0c +  m0r +  (m0ps1 +  m0iex1 +  m0ps2 + m0iex2) + m0extra #optimized for conditionals
    
    return ntot, mtot, m0tot 



def boxmuller(storage, nit):
    """
    Computes the number of inputs and multiplications of
    the box muller sampling method
    
    Parameters:
    -----------
    storage:  storage of our fractional representation
    nit: number of iterations of cordic
    
    Returns: 
    --------
    n: number of inputs
    m: number of integer multiplications between two private values
    m0: the number of multiplication gates that are 0 
    """ 
    
    nm, mm, m0m = modsum(storage)
    nlog, mlog, m0log = clogGen(storage, nit)
    nsqrt, msqrt, m0sqrt = csqrtGen(storage, nit, 2 * storage * m.log(2))
    nprod, mprod, m0prod = fracprod(storage)
    ntrig, mtrig, m0trig = ctrigGen(storage, nit)
    
    nextra = -6 # -1 log -1 sqrt, -1 prod, -1 a_pi  -2 prod

    ntot = 2* nm + nlog + nsqrt + 3* nprod + ntrig + nextra
    mtot = 2*mm + mlog + msqrt + 3*mprod + mtrig 
    m0tot = 2*m0m + m0log + m0sqrt + 3*m0prod + m0trig 


    return ntot, mtot, m0tot


def polarmethod(storage, nit):
    """
    Computes the number of inputs and multiplications of
    the box muller sampling method
    
    Parameters:
    -----------
    storage:  storage of our fractional representation
    nit: number of iterations of cordic
    
    Returns: 
    --------
    - number of inputs
    - number of integer multiplications between two private values
    m0: the number of multiplication gates that are 0 
    """ 
    nm, mm, m0m = modsum(storage+1)
    nprod, mprod, m0prod = fracprod(storage)
    nrng, mrng, m0rng = grange(1, 2**storage-1)
    ndiv, mdiv, m0div = fracdivp(storage)
    nlog, mlog, m0log = clogGen(storage, nit) 
    nsqrt, msqrt, m0sqrt = csqrtGen(storage, nit,  2**(storage+1) * storage * m.log(2))
    
    nextra = -12 # -4 prod -1 log -2 div -1 sqrt -4 prod 
    inputs = 2*nm + 4 * nprod + nrng + ndiv + nlog +  nsqrt 
    multgates = 2*mm + 4 * mprod + mrng + mdiv + mlog +  msqrt
    mult0gates =  2*m0m + 4 * m0prod + m0rng + m0div + m0log +   m0sqrt
    
    return inputs, multgates, mult0gates
    
    
def cltmethod(storage, nt):
    """
    Computes the number of inputs and multiplications of
    the central limit sampling method
    
    Parameters:
    -----------
    storage:  storage of our fractional representation
    nt: number uniform terms
    
    Returns: 
    --------
    - number of inputs
    - number of integer multiplications between two private values
    m0: the number of multiplication gates that are 0 
    """ 
    nmod, mmod, m0mod = modsum(storage)
    
    return nt * nmod, nt * mmod, nt * m0mod

def erfc(L, storage, gsize): 
    """
    Computes a lower bound in the number of inputs and multiplications of
    the computation of erfc approximation 
    
    Parameters:
    -----------
    storage:  storage of our fractional representation
    L: number of approximation terms
    gsize: group size
    
    Returns: 
    --------
    - number of inputs
    - number of integer multiplications between two private values
    m0: the number of multiplication gates that are 0 
    """ 
    nd, md, m0d = fracdivp(gsize) 
    np, mp, m0p = fracprod(storage) 
    
    return L * (nd-2) + np, L * md + mp, L * m0d + m0p
    
def fracpublicdiv(b): 
    """
    Computes a lower bound in the number of inputs and multiplications of
    a proof of division c = a/b, where b is public, and a, c private. 
    
    Parameters:
    -----------
    b: public divisor
    
    
    Returns: 
    --------
    - number of inputs
    - number of integer multiplications between two private values
    m0: the number of multiplication gates that are 0 
    """ 
    nr, mr, m0r = grange(-b,b) 
    
    return nr +1, mr, m0r
    
def erf(L, storage): 
    """
    Computes a lower bound in the number of inputs and multiplications of
    the computation of erf approximation 
    
    Parameters:
    -----------
    L: number of approximation terms
    storage:  storage of our fractional representation
    
    
    Returns: 
    --------
    A tuple (n, m, m0)
    n: number of inputs
    m: number of integer multiplications between two private values
    m0: the number of multiplication gates that are 0 
    """ 
    
    ncum = 0
    mcum = 0 
    m0cum = 0
    
    np, mp, m0prod = fracprod(storage)
    
    ncum += (2 + L) * (np-2) 
    mcum += (2 + L) * mp 
    m0cum += (2 + L) * m0prod
    
    for l in range(L):
        ndi1, mdi1, m0di1 = fracpublicdiv(l+1)
        ndi2, mdi2, m0di2 = fracpublicdiv(2*(l+1)+1)
        ncum += ndi1 + ndi2
        mcum += mdi1 + mdi2
        m0cum += m0di1 + m0di2
        
    return ncum, mcum, m0cum
    
def erf_complete(prec, storage, gsize): 
    """
    Computes the number of inputs and multiplications of
    the computation of erf or erfc approximation 
    for a given precision
    
    Parameters:
    -----------
    prec: desired precision for erf
    storage:  storage of our fractional representation
    gsize: cryptographic group size
    
    
    Returns: 
    --------
    A pair (n, m)
    n: number of inputs
    m: number of integer multiplications between two private values
    m0: the number of multiplication gates that are 0 
    """ 
    assert(storage >=  2 *prec)
    
    lBinv = m.log(2) * prec # ln(1/B)
    Lerf = m.ceil( 2.36 * lBinv + 2.15) 
    Lerfc = m.floor( lBinv /2 + 1.288) 
    
    nr, mr, m0r = rangep(storage) # 2 range proofs for bounds on y, in total it requires @storage bits
    
    nerf, merf, m0erf = erf(Lerf, storage)
    nerfc, merfc, m0erfc = erfc(Lerfc, storage, gsize)
    
    mextra = 5 # control multiplications
    
    return nerf + nerfc + nr, merf + merfc + mr,  m0erf + m0erfc + m0r 
    
    
def erfinvSP_comp():
    """
    This counts the cost of a circuit for the erfinv
    method described in (Mike Giles, Computing Gems) paper
    on erfinv. 
    The algorithm is for the erfinv_SP approximation
    returns n= number of inputs, m=multiplications, m0 = mult. gates equal to 0
    """
    
    singleprec = 23 # single precision: 23 bits 
    ln_it = 23 # iterations for ln (iterations or precision?)
    ln_stor = 28  # storage for log
    sqrt_it = 21 # iterations for sqrt
    sqrt_stor = 26 # storage for sqrt 
    
    nl, ml, m0l = clogGen(ln_stor, ln_it)  # to compute #\log(1-x^2) 
    # to compute sqrt(-ln(1-x^2)) 
    #   sqrt should be generalized to the interval (0, - ln(2^-singleprec))
    #   equivalent to (0, singleprec * ln(2) )
    ns, ms, m0s = csqrtGen(sqrt_stor, sqrt_it,  2 * ln_stor * m.log(2))  
    
    nr, mr, m0r = fracprod(singleprec) 
    nprod = 20 # we evaluate p1 and p2 polynomials of degree 8, we do it as in page 9 of
                # the paper
    
    n = nl + ns + nprod * nr
    mtot = ml + ms + nprod * mr
    m0 = m0l + m0s + nprod * m0r
    
    
    return n, mtot, m0 

def costtheorem4(inputs, multgates, m0mult):
    """
    Computes the communication cost of a circuit proof given its inputs 
    and multiplication gates
    It returns only the elements that the prover sends, 
    as we assume we use the Fiat-Shamir heursitc, so challenges
    are computed from previous messages
    
    Parameters:
    ----------
    inputs: number of inputs of the circuit
    multgates: numger of non-constant multiplication gates of the circuit
    m0mult: the number of multiplication gates that are 0 
    
    Returns: 
    g: number of cryptographic group elements 
    -------
    """
    GZp = 3 # Group elements by the extra 6 elements of Zp in the theorem 
    
    
    return 2 * m.ceil(m.log2(inputs + 2*multgates - m0mult + 4)) + GZp
    

def nexplin(inputs):
    """
    Computes the number of Group Exponentiations cost of 
    a compressed linear opening proof of numbe if inputs
    equal to @inputs 
    
    Parameters:
    ----------
    inputs: number of inputs of the linear function

    
    Returns: a tuple p,v 
    p: number of group exponentiations a Prover does
    v: number of group exponentiations a Verifier does
    -------
    """
    mu = m.ceil(m.log2(inputs + 1)) - 2
    pcost = 4 * inputs + 2 * mu
    vcost = 3 + inputs + 2 * mu
    
    return pcost, vcost
    
    
def nexpcircuit(inputs, multgates, m0mult): 
    """
    Computes the number of Group Exponentiations cost of a circuit proof 
    given its inputs and multiplication gates
    
    Parameters:
    ----------
    inputs: number of inputs of the circuit
    multgates: numger of non-constant multiplication gates of the circuit
    m0mult: the number of multiplication gates that are 0 
    
    Returns: a tuple p,v 
    p: number of group exponentiations a Prover does
    v: number of group exponentiations a Verifier does
    -------
    """
    veclength = inputs + 2* multgates + 3 - m0mult
    
    plin, vlin = nexplin(veclength)  
    
    p = veclength + 1 +  plin
    v = vlin 
    
    return p, v 
    
def costboxmuller(nit, storage):

    inp, mgates, m0gates = boxmuller(storage, nit)
    expprov, expverif  = nexpcircuit(inp, mgates,m0gates)
    return costtheorem4(inp, mgates, m0gates),  (expprov, expverif) 
    
def costpolarm(nit, storage):

    inp, mgates, m0gates = polarmethod(storage, nit)
    expprov, expverif  = nexpcircuit(inp, mgates,m0gates)
    return costtheorem4(inp, mgates, m0gates) ,  (expprov, expverif) 
    
    return costtheorem4(inp, mgates, m0gates), nexpcircuit(inp, mgates, m0gates) 
    
def costclt(nt, storage):
    inp, mgates, m0gates = cltmethod(storage, nt)
    return costtheorem4(inp, mgates, m0gates), nexpcircuit(inp, mgates, m0gates) 
    
def costerf(prec, storage, gsize):
    inp, mgates, m0gates = erf_complete(prec, storage, gsize)
    return costtheorem4(inp, mgates, m0gates), nexpcircuit(inp, mgates, m0gates) 
    
def costerfinv_SP():
    n, m, m0 = erfinvSP_comp()
    return  costtheorem4(n, m, m0), nexpcircuit(n, m, m0) 
    


## ((precision, storage), mse), 
## ((terms, storage),mse) -> CLT    
              

boxmuller_mse = [  ((2, 4),  0.325718), 
                   ((3,5), 0.139677),
                   ((4,6), 0.0510693),
                   ((5,8), 0.0123098),
                   ((6, 9), 0.00332539),
                   ((7, 10),  0.00088848),
                   ((8, 11), 0.000218205),
                   ((9,13), 4.56329e-05),
                   ((10,14), 1.10287e-05),
                   ((11,15),  4.17147e-06),
                   ((12,16),  2.49443e-06),
                   ((13,17),  1.27068e-06),
                   ((14,19), 3.05401e-07)
                ] 
                    

                             
polar_mse = [   (   (2,4),      0.177642    ),
                (   (3,5),      0.0540751   ),
                (   (4,6),      0.0150116   ),
                (   (5,8),      0.00404098  ),
                (   (6, 9),     0.00108797  ),
                (   (7, 10),    0.00027652  ),
                (   (8, 11),    7.88125e-05 ),
                (   (9, 13),    2.15976e-05 ),
                (   (10, 14),   4.07184e-06 ),
                (   (11, 15),   1.5316e-06  ),
                (   (12, 16),   2.70353e-07 )
            ]          
            
        

                 
clt_mse = [    (   (2, 6),     0.0102134),  
                (   (4, 8),     0.00204786), 
                (   (8, 10),    0.000524351),  
                (   (12, 12),   0.000174222),
                (   (14, 14),   8.9647e-05),     
                (   (16, 15),   6.291e-05),
                (   (18, 15),   5.2031e-05),
                (   (20, 15),   4.18719e-05), 
                (   (25, 15),   2.91095e-05), 
                (   (50, 20),   6.45254e-06),
                (   (100, 20),  1.78406e-06), 
                (   (200, 30),  6.69664e-07), 
                (   (400, 50),   3.22656e-07)
            ] 
            
            
erf_mse = [    ((1,2),     0.678118),
                ((2,4),     0.298359),
                ((3,6),     0.167774),
                ((4,8),     0.0667235),
                ((5,10),    0.0226246),
                ((6,12),    0.0121341),
                ((7,14),    0.00611903),
                ((8,16),    0.00257961),
                ((9,18),    0.000898716),
                ((10,20),   0.000298932),
                ((11,22),   0.000120407),
                ((12,24),   6.43528e-05),
                ((13,26),   2.94934e-05),
                ((14,28),   1.49298e-05),
                ((15,30),   8.54038e-06),
                ((16,32),   5.94478e-06),
                ((17,34),   2.5089e-06),
                ((18,36),   1.33425e-06),
                ((19,38),   6.51183e-07),
                ((20,40),   5.37009e-07) 
            ]
            
erfinvSP_mse =  3.49113e-07
              

"""
From here, we generate reproducible experiments, where hardcoded
values can be generated functions within this document 
""" 
    
import subprocess 
import re   
import sys

           

bmpoints =   [  (2, 4), 
                (3, 5), 
                (4, 6), 
                (5, 8), 
                (6, 9), 
                (7, 10), 
                (8, 11), 
                (9, 13), 
                (10, 14), 
                (11, 15), 
                (12, 16), 
                (13, 17), 
                (14, 19)
            ]
                    

        
cltpoints = [(2, 6), (4, 8), (8, 10), (12, 12), (14, 14), (16, 15), (18, 15), (20, 15), (25, 15), (50, 20), (100, 20), (200, 30), (400, 50)]

erfpoints = [(1, 2), (2, 4), (3, 6), (4, 8), (5, 10), (6, 12), (7, 14), (8, 16), (9, 18), (10, 20), (11, 22), (12, 24), (13, 26), (14, 28), (15, 30), (16, 32), (17, 34), (18, 36), (19, 38), (20, 40)]


def genBMpoints(points,seed): 
    """ 
    Generates mse for a list of pairs of points (it, stor) in the input
    """
    outpoints = [] 
    samples=10000000
    
    for (it, stor) in points: 
        print('computing point (n=', it, ', storage=',  stor, ')') 
        
        p1 = subprocess.Popen(["./simu", "b", str(it), str(stor), str(samples), str(seed) ], stdout=subprocess.PIPE)
        p2 = subprocess.Popen(["./stats", "asd"], stdin=p1.stdout, stdout=subprocess.PIPE)
        p1.stdout.close()
        out, err = p2.communicate()
        drparams = re.findall(r'[-+]?(?:\d*\.\d+|\d+)', out.decode("utf-8")  )
        if(len(drparams) == 4):
            mse = float(drparams[-2]+ 'e'+ drparams[-1])
        else: 
            mse = float(drparams[-1]) 
            
        outpoints.append( ((it,stor), mse))
        
    return outpoints 
    



"""
genBMpoints_hardcoded =  genBMpoints(bmpoints, seed=123)
"""
genBMpoints_hardcoded = [   ((2, 4), 0.325641), 
                            ((3, 5), 0.139546), 
                            ((4, 6), 0.0509689), 
                            ((5, 8), 0.0123991), 
                            ((6, 9), 0.00337924), 
                            ((7, 10), 0.000905946), 
                            ((8, 11), 0.000227178), 
                            ((9, 13), 5.35005e-05), 
                            ((10, 14), 3.41321e-05), 
                            ((11, 15), 8.44912e-06), 
                            ((12, 16), 1.47869e-06), 
                            ((13, 17), 1.07226e-06), 
                            ((14, 19), 3.29921e-07)
                        ]

    
def genPolarpoints(points, seed): 
    """ 
    Generates mse for a list of pairs of points (it, stor) in the input
    """
    outpoints = [] 
    samples=10000000
    
    for (it, stor) in points: 
        print('computing point (n=', it, ', storage=',  stor, ')') 
        
        p1 = subprocess.Popen(["./simu", "p", str(it), str(stor), str(samples), str(seed) ], stdout=subprocess.PIPE)
        p2 = subprocess.Popen(["./stats", "asd"], stdin=p1.stdout, stdout=subprocess.PIPE)
        p1.stdout.close()
        out, err = p2.communicate()
        drparams = re.findall(r'[-+]?(?:\d*\.\d+|\d+)', out.decode("utf-8")  )
        if(len(drparams) == 4):
            mse = float(drparams[-2]+ 'e'+ drparams[-1])
        else: 
            mse = float(drparams[-1]) 
            
        outpoints.append( ((it,stor), mse))
        
    return outpoints 
    

polarpoints  =  [   (2, 4), 
                    (3, 5), 
                    (4, 6), 
                    (5, 8), 
                    (6, 9), 
                    (7, 10), 
                    (8, 11), 
                    (9, 13), 
                    (10, 14), 
                    (11, 15), 
                    (12, 16),
                    # ~ (13, 17),
                    (14, 19) 
                ]


"""
genPolarpoints_harcoded = genPolarpoints(polarpoints, 123)
"""    
# ~ genPolarpoints_harcoded =  [    ((2, 4), 0.177651), 
                                # ~ ((3, 5), 0.0541007), 
                                # ~ ((4, 6), 0.0150467), 
                                # ~ ((5, 8), 0.00400444), 
                                # ~ ((6, 9), 0.00108977), 
                                # ~ ((7, 10), 0.000253169), 
                                # ~ ((8, 11), 7.57672e-05), 
                                # ~ ((9, 13), 1.98489e-05), 
                                # ~ ((10, 14), 1.70189e-05), 
                                # ~ ((11, 15), 5.73563e-06), 
                                # ~ ((12, 16), 1.40169e-06),
                                # ~ ((13, 17), 6.50945e-07), 
                            # ~ ]
                            
"""
genPolarpoints_harcoded = genPolarpoints(polarpoints, 456)
"""   
genPolarpoints_harcoded =   [ ((2, 4), 0.177642), 
                              ((3, 5), 0.0540751), 
                              ((4, 6), 0.0150116), 
                              ((5, 8), 0.00404098), 
                              ((6, 9), 0.00108797), 
                              ((7, 10), 0.00027652), 
                              ((8, 11), 7.88125e-05), 
                              ((9, 13), 2.15976e-05), 
                              ((10, 14), 1.66795e-05), 
                              ((11, 15), 4.877e-06), 
                              ((12, 16), 7.83477e-07), 
                              # ~ ((13, 17), 1.0361e-06), 
                              ((14, 19), 3.29138e-07)  
                            ]

    

    
def genCLTpoints(points, seed): 
    """ 
    Generates mse for a list of pairs of points (it, stor) in the input
    """
    outpoints = [] 
    samples=10000000
    
    for (terms, stor) in points: 
        print('computing point (n=', terms, ', storage=',  stor, ')') 
        
        p1 = subprocess.Popen(["./simu", "c", str(terms), str(stor), str(samples), str(seed) ], stdout=subprocess.PIPE)
        p2 = subprocess.Popen(["./stats", "asd"], stdin=p1.stdout, stdout=subprocess.PIPE)
        p1.stdout.close()
        out, err = p2.communicate()
        drparams = re.findall(r'[-+]?(?:\d*\.\d+|\d+)', out.decode("utf-8")  )
        if(len(drparams) == 4):
            mse = float(drparams[-2]+ 'e'+ drparams[-1])
        else: 
            mse = float(drparams[-1]) 
            
        outpoints.append( ((terms,stor), mse))
        
    return outpoints
    
genCLTpoints_hardcoded = [((2, 6), 0.0102399), ((4, 8), 0.00204671), ((8, 10), 0.000535236), ((12, 12), 0.000161256), ((14, 14), 8.56591e-05), ((16, 15), 6.36462e-05), ((18, 15), 5.047e-05), ((20, 15), 4.1034e-05), ((25, 15), 3.24533e-05), ((50, 20), 5.28156e-06), ((100, 20), 3.28136e-06), ((200, 30), 7.82954e-07), ((400, 50), 4.37378e-07)]

    
def genERFSeriespoints(points, seed):
    """ 
    Generates mse for a list of pairs of points (it, stor) in the input
    """
    outpoints = [] 
    samples=10000000
    
    for (prec, stor) in points: 
        print('computing point (n=', prec, ', storage=',  stor, ')') 
        
        p1 = subprocess.Popen(["./simu", "g", str(prec), str(stor), str(samples), str(seed) ], stdout=subprocess.PIPE)
        p2 = subprocess.Popen(["./stats", "asd"], stdin=p1.stdout, stdout=subprocess.PIPE)
        p1.stdout.close()
        out, err = p2.communicate()
        drparams = re.findall(r'[-+]?(?:\d*\.\d+|\d+)', out.decode("utf-8")  )
        if(len(drparams) == 4):
            mse = float(drparams[-2]+ 'e'+ drparams[-1])
        else: 
            mse = float(drparams[-1]) 
            
        outpoints.append( ((prec,stor), mse))
        
    return outpoints
    
genERFSeriespoints_hardcoded = [((1, 2), 0.6781), ((2, 4), 0.298321), ((3, 6), 0.16768), ((4, 8), 0.0668842), ((5, 10), 0.0225742), ((6, 12), 0.0120879), ((7, 14), 0.00613058), ((8, 16), 0.00259996), ((9, 18), 0.000914042), ((10, 20), 0.000300951), ((11, 22), 0.000124214), ((12, 24), 6.3845e-05), ((13, 26), 2.73241e-05), ((14, 28), 1.41521e-05), ((15, 30), 8.08048e-06), ((16, 32), 5.90614e-06), ((17, 34), 2.65669e-06), ((18, 36), 1.59764e-06), ((19, 38), 7.65903e-07), ((20, 40), 5.7581e-07)]

    
def genRatErfinvSPpoint(seed): 
    samples=10000000
    p1 = subprocess.Popen(["./simu", "s", str(samples), str(seed) ], stdout=subprocess.PIPE)
    p2 = subprocess.Popen(["./stats", "asd"], stdin=p1.stdout, stdout=subprocess.PIPE)
    p1.stdout.close()
    out, err = p2.communicate()
    drparams = re.findall(r'[-+]?(?:\d*\.\d+|\d+)', out.decode("utf-8")  )
    if(len(drparams) == 4):
        mse = float(drparams[-2]+ 'e'+ drparams[-1])
    else: 
        mse = float(drparams[-1]) 
    
    return mse
    
    
def plot_GEl(): 
    plt.yscale('log', base=2)
    # ~ plt.xscale('log')
    plt.xlabel('Group Elements')
    plt.ylabel('Mean Squared Error')
    msize=5  # point size in the plot
    lwidth=2

    gz = 128 
    
    erfinvpoint = genRatErfinvSPpoint(456)  # rational erfinv with single precision
    
    bmptsn = [0, 5, 9, 12]
    # ~ bmptsn = range(13)
    bmpts_all = genBMpoints(bmpoints, 123)
    bmpts = [ bmpts_all[i] for i in bmptsn] 
    bmx = [ costboxmuller(ni, st)[0] for ( (ni, st), _ ) in bmpts   ] 
    bmy = [ mse  for ( _, mse ) in bmpts   ] 
    
    pmptsn = [1,5, 10, 11] 
    # ~ pmptsn = range(12)
    pmpts_all = genPolarpoints(polarpoints, 456)
    pmpts = [ pmpts_all[i] for i in pmptsn ] 
    pmx = [ costpolarm(ni, st)[0] for ( (ni, st), _ ) in pmpts  ] 
    pmy = [ mse for ( _, mse ) in pmpts  ] 
   
    cltptsn = [0,1,3,8, 9, 10, 11, 12] 
    cltpts_all = genCLTpoints(cltpoints, 456)
    cltpts = [ cltpts_all[i] for i in cltptsn] 
    cltx = [ costclt(ni, st)[0] for ( (ni, st), _ ) in cltpts  ] 
    clty = [ mse for ( _, mse ) in cltpts  ] 
    
    erfptsn = [1,6, 12, 19 ] 
    erfpts_all = genERFSeriespoints(erfpoints, 456)
    erfpts = [ erfpts_all[i] for i in erfptsn ] 
    erfx = [ costerf(pr, st, gz)[0] for ( (pr, st), _ ) in erfpts ] 
    erfy = [ mse for ( _ , mse ) in  erfpts ] 
    
    plt.plot(bmx , bmy,'-^', label='BM', linewidth=lwidth, markersize= msize)
    plt.plot(pmx, pmy,'-*', label='PolM', linewidth=lwidth,  markersize= msize)
    plt.plot(cltx, clty,'-o', label='CLT', linewidth=lwidth,  markersize= msize)
    plt.plot(erfx, erfy, '-d', label='InvM-S', linewidth=lwidth)
    plt.plot([  costerfinv_SP()[0]] , [ erfinvpoint ], 'X', label='InvM-R')
    plt.legend()
    plt.show()
    
    
def plot_GEl_Classic_PolM(): 
    plt.yscale('log',base=2)
    plt.xscale('log',base=2)
    # ~ plt.xscale('log')
    plt.xlabel('Group Elements')
    plt.ylabel('Mean Squared Error')
    msize=5  # point size in the plot
    lwidth=2

    gz = 128 

    
    pmptsn = [1,5, 10, 11] 
    # ~ pmptsn = range(12)
    pmpts_all  = genPolarpoints(polarpoints, 456)
    pmpts = [ pmpts_all[i] for i in pmptsn ] 
    
    pmx = [ costpolarm(ni, st)[0] for ( (ni, st), _ ) in pmpts  ] 
    pmy = [ mse for ( _, mse ) in pmpts  ] 
   
    pmx_classic = [ clsp.cpm_cost(ni, st)[0] for ( (ni, st), _ ) in pmpts  ] 

    plt.plot(pmx, pmy,'-*', label='PolM - Compressed', linewidth=lwidth,  markersize= msize)
    plt.plot(pmx_classic, pmy,':*', label='PolM - Classic', linewidth=lwidth,  markersize= msize)
    plt.legend()
    plt.show()
    
def plot_GEl_Classic_CLT(): 
    plt.yscale('log', base=2)
    plt.xscale('log', base=2)
    # ~ plt.xscale('log')
    plt.xlabel('Group Elements')
    plt.ylabel('Mean Squared Error')
    msize=5  # point size in the plot
    lwidth=2

    cltptsn = [0,1,3,8, 9, 10, 11, 12]
    cltpts_all = genCLTpoints(cltpoints, 456)
    cltpts = [ cltpts_all[i] for i in cltptsn] 
    cltx = [ costclt(ni, st)[0] for ( (ni, st), _ ) in cltpts  ] 
    clty = [ mse for ( _, mse ) in cltpts  ] 
       
    cltx_classic = [ clsp.clt_cost(nt, st)[0] for ( (nt, st), _ ) in cltpts  ] 
	# ~ pmptsn = [1,5, 10, 11] 
    pmptsn = range(12)
    # ~ pmpts = [ genPolarpoints_harcoded[i] for i in pmptsn ] 
    
    # ~ pmx = [ costpolarm(ni, st)[0] for ( (ni, st), _ ) in pmpts  ] 
    # ~ pmy = [ mse for ( _, mse ) in pmpts  ] 
   
    # ~ pmx_classic = [ clsp.cpm_cost(ni, st)[0] for ( (ni, st), _ ) in pmpts  ] 

    # ~ plt.plot(pmx, pmy,'-*', label='PolM - Compressed', linewidth=lwidth,  markersize= msize)
    # ~ plt.plot(pmx_classic, pmy,':*', label='PolM - Classic', linewidth=lwidth,  markersize= msize)
   

    plt.plot(cltx, clty,'-o', label='CLT - Compressed', linewidth=lwidth,  markersize= msize)
    plt.plot(cltx_classic, clty,':o', label='CLT - Classic', linewidth=lwidth,  markersize= msize)
    plt.legend()
    plt.show()
    
def plot_GExp(idx): 
    """ 
    Plots the computational cost of BM, PolM, CLT and InvM 
    
    Parameters:
    ----------
    idx: equals 0 if printing prover cost, or 1 if printing verifier cost
    
    """ 
    
    plt.yscale('log', base=2)
    plt.xscale('log', base=2)
    # ~ plt.xscale('log')
    plt.xlabel('Group Exponentiations')
    plt.ylabel('Mean Squared Error')
    msize=5  # point size in the plot
    lwidth=2

    gz = 128 
    
    bmptsn = [0, 3, 7, 12]
    
    # ~ bmpts = [ boxmuller_mse[i] for i in bmptsn ]
    
    bmpts_all = genBMpoints(bmpoints, 123)
    bmx = [costboxmuller(ni, st)[1][idx] for ( (ni, st), _ ) in bmpts_all ] 
    bmy = [ mse  for ( _, mse ) in  bmpts_all  ] 
    
    pmptsn = [0,3, 9,10] 
    # ~ pmpts = [ polar_mse[i] for i in pmptsn ] 
    pmpts_all = genPolarpoints(polarpoints, 456)
    pmx = [  costpolarm(ni, st)[1][idx] for ( (ni, st), _ ) in pmpts_all ] 
    pmy = [ mse for ( _, mse ) in pmpts_all ] 
   
    cltptsn = [0,1,2,3,4,5,8,9,10,11,12] 
    cltpts_all = genCLTpoints(cltpoints, 456)
    cltpts = [ cltpts_all[i] for i in cltptsn] 
    cltx = [ costclt(ni, st)[1][idx] for ( (ni, st), _ ) in cltpts  ] 
    clty = [ mse for ( _, mse ) in cltpts  ] 
    
    erfptsn = [1,6, 12, 19 ] 
    
    erfpts_all = genERFSeriespoints(erfpoints, 456)
    erfpts = [  erfpts_all[i] for i in erfptsn ] 
    
    erfx = [ costerf(pr, st, gz)[1][idx] for ( (pr, st), _ ) in erfpts_all ] 
    erfy = [ mse for ( _ , mse ) in  erfpts_all ] 
    
    plt.plot(bmx , bmy,'-^', label='BM', linewidth=lwidth, markersize= msize)
    plt.plot(pmx, pmy,'-*', label='PolM', linewidth=lwidth,  markersize= msize)
    plt.plot(cltx, clty,'-o', label='CLT', linewidth=lwidth,  markersize= msize)
    plt.plot(erfx, erfy, '-d', label='InvM-S', linewidth=lwidth)
    plt.plot([  costerfinv_SP()[1][idx] ] , [ genRatErfinvSPpoint(456) ], 'X', label='InvM-R')
    plt.legend()
    plt.show()  
    
def plot_GExp_prov_zoom(): 
    """ 
    Plots the computational cost of BM, PolM, CLT 
    
    """ 
    
    plt.yscale('log',base=2)
    # ~ plt.xscale('log')
    plt.xlabel('Group Exponentiations')
    plt.ylabel('Mean Squared Error')
    msize=5  # point size in the plot
    lwidth=2

    gz = 128 
    
    bmpts_all = genBMpoints(bmpoints, 123)
    
    bmx = [costboxmuller(ni, st)[1][0] for ( (ni, st), _ ) in  bmpts_all ] 
    bmy = [ mse  for ( _, mse ) in bmpts_all  ] 
    
    pmpts_all = genPolarpoints(polarpoints, 456)
    pmx = [  costpolarm(ni, st)[1][0] for ( (ni, st), _ ) in pmpts_all  ] 
    pmy = [ mse for ( _, mse ) in pmpts_all ] 
   
    cltptsn = range(10) 
    cltpts_all = genCLTpoints(cltpoints, 456)
    cltpts = [ cltpts_all[i] for i in cltptsn] 
    cltx = [ costclt(ni, st)[1][0] for ( (ni, st), _ ) in cltpts  ] 
    clty = [ mse for ( _, mse ) in cltpts  ] 
    
    plt.xlim([0, 12500])
    plt.plot(bmx , bmy,'-^', label='BM', linewidth=lwidth, markersize= msize)
    plt.plot(pmx, pmy,'-*', label='PolM', linewidth=lwidth,  markersize= msize)
    plt.plot(cltx, clty,'-o', label='CLT', linewidth=lwidth,  markersize= msize)
    # ~ plt.plot([  costerfinv_SP()[1][0] ] , [ erfinvSP_mse ], 'X', label='InvM-R')
    # ~ plt.plot([  costerfinv_SP()[1][idx] ] , [ erfinvSP_mse ], 'X', label='InvM - Rational')
    # ~ plt.plot(erfx, erfy, '-d', label='InvM', linewidth=lwidth)
    # ~ plt.plot([ erfinvSP_comp(128)] , [ 3.49113e-07], 'X', label='InvM - Rational')
    plt.legend()
    plt.show() 
    
def plot_GExp_verif_zoom(): 
    """ 
    Plots the computational cost of BM, PolM, CLT
    
    Parameters:
    ----------
    idx: equals 0 if printing prover cost, or 1 if printing verifier cost
    
    """ 
    
    plt.yscale('log',base=2)
    # ~ plt.xscale('log')
    plt.xlabel('Group Exponentiations')
    plt.ylabel('Mean Squared Error')
    msize=5  # point size in the plot
    lwidth=2

    gz = 128 
    
    bmpts_all = genBMpoints(bmpoints, 123)
    bmx = [costboxmuller(ni, st)[1][1] for ( (ni, st), _ ) in bmpts_all ] 
    bmy = [ mse  for ( _, mse ) in bmpts_all  ] 
    
    
    pmpts_all = genPolarpoints(polarpoints, 456)
    pmx = [  costpolarm(ni, st)[1][1] for ( (ni, st), _ ) in pmpts_all ] 
    pmy = [ mse for ( _, mse ) in pmpts_all] 
   
    cltptsn = range(10) 
    cltpts_all = genCLTpoints(cltpoints, 456)
    cltpts = [ cltpts_all[i] for i in cltptsn] 
    cltx = [ costclt(ni, st)[1][1] for ( (ni, st), _ ) in cltpts  ] 
    clty = [ mse for ( _, mse ) in cltpts  ] 
    
    plt.xlim([0, 2600])
    plt.plot(bmx , bmy,'-^', label='BM', linewidth=lwidth, markersize= msize)
    plt.plot(pmx, pmy,'-*', label='PolM', linewidth=lwidth,  markersize= msize)
    plt.plot(cltx, clty,'-o', label='CLT', linewidth=lwidth,  markersize= msize)
    
    # ~ plt.plot(erfx, erfy, '-d', label='InvM', linewidth=lwidth)
    # ~ plt.plot([ erfinvSP_comp(128)] , [ 3.49113e-07], 'X', label='InvM - Rational')
    plt.legend()
    plt.show() 
    
