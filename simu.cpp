// Compile with 
// g++ -o simu  simu.cpp -lmpfr -lgmp 

#include <stdio.h> 
#include <math.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <gmp.h>
#include <mpfr.h>

#include <random> 
#include <iostream>
#include <fstream> 
#include <iomanip>
#include <limits> 
#include <boost/math/special_functions/erf.hpp>


/** 
 * Generates a random number inside rop
 * rop should be initialized to nbin prec 
 */ 
void ugen(mpfr_t rop, mpfr_prec_t nbins, gmp_randstate_t state)
{
    mpfr_urandomb(rop, state); 
}

/** 
* get precision from the number of iterations in linear cordic
*/ 
int cordicprec_circ(int it)  
{ 
    return it-1;
} 
    

/** 
* get precision from the number of iterations in hyperbolic cordic
*/
int cordicprec_hyp(int it)  
{ 
    int k = int(ceil( log2( 2.0 * double(it))  / double(log2(3.0)))) -1; 
    while (pow(3.0, double(k+1)) + 2.0 * double(k) - 1.0 > double(it))
        k--; 

    return it- k -1;
}

/**
 *  Run box muller generating one sample, x1 and x2 mpfr variables
 *  must be already initialized
 *  initialized
 **/  
void boxmuller(mpfr_t x1, mpfr_t x2, 
                            int it, int storage, gmp_randstate_t gst) 
{
    int preccirc = cordicprec_circ(it);
    int prechyp = cordicprec_hyp(it); 
    // make sure that the storage is enough
    assert(storage > preccirc + int(log(((long double)preccirc))+1)); 
    assert(storage > prechyp + int(log(((long double)prechyp))+1)); 

    mpfr_t u, pi, f, g; 
    mpfr_inits2(storage, u, pi, (mpfr_ptr) NULL); 
    mpfr_init2(f, prechyp);
    mpfr_init2(g, preccirc);
    do { // sample u1
        mpfr_urandomb(u, gst);
    } while (mpfr_zero_p(u));  // resample if u=0
    // DEBUG
    //~ fprintf(stdout, "u1="); 
    //~ mpfr_out_str(stdout, 10, 0, u, MPFR_RNDZ); 
    //~ fprintf(outp, "\n");
    // compute f    
    mpfr_log(f, u, MPFR_RNDZ); 
    //DEBUG
    //~ if (mpfr_inf_p(f)) { 
        //~ fprintf(stdout, "u1="); 
        //~ mpfr_out_str(stdout, 10, 0, u, MPFR_RNDZ); 
        //~ fprintf(outp, "\n");
    //~ }
    // we use u again below to economize an initialization
    mpfr_mul_si(u, f, -2, MPFR_RNDZ); 
    mpfr_sqrt(f, u, MPFR_RNDZ); 
    // compute g1 and x1
    do { // sample u2
        mpfr_urandomb(u, gst);
    } while (mpfr_zero_p(u));  // resample if u=0
    mpfr_const_pi(pi, MPFR_RNDZ); 
    mpfr_mul(u, pi, u, MPFR_RNDZ); 
    mpfr_mul_ui(u, u, 2, MPFR_RNDZ);  // u <- 2 * pi * u
    mpfr_cos(g, u, MPFR_RNDZ);
    mpfr_mul(x1, f, g, MPFR_RNDZ); 
    // compute g2 and x2
    mpfr_sin(g, u, MPFR_RNDZ);
    mpfr_mul(x2, f, g, MPFR_RNDZ); 
    // clear variables 
    mpfr_clears(u, pi, f, g, (mpfr_ptr) NULL); 
}



void bmgen(int it, int storage, int samples, int sd, FILE* outp)
{
    /**
    * Generates many boxmuller samples
    */
    // start and seed the random number generator
    gmp_randstate_t st; 
    gmp_randinit_default(st); 
    gmp_randseed_ui(st, sd); 

    mpfr_t x1, x2; 
    mpfr_inits2(storage, x1, x2, (mpfr_ptr) NULL); 
    for (int i = 0; i < samples/2 ; i++) { 
        boxmuller(x1, x2, it, storage, st)  ;
        // print
        mpfr_out_str(outp, 10, 0, x1, MPFR_RNDZ); 
        fprintf(outp, "\n"); 
        mpfr_out_str(outp, 10, 0, x2, MPFR_RNDZ);
        fprintf(outp, "\n"); 
    }
    //glear varaibles
    mpfr_clears(x1, x2, (mpfr_ptr) NULL);
    gmp_randclear(st); 
}

/**
 * Polar Method
 */ 
int polarm(mpfr_t y1, mpfr_t y2, int it, int st, gmp_randstate_t gst)
{
    
    int phyp = cordicprec_hyp(it);
    int gen = 0;
    
    mpfr_t v1, v2, alpha, a1,  t;
    mpfr_inits2(st, v1, v2, alpha, a1, (mpfr_ptr) NULL); 
    mpfr_init2(t, phyp); 
    do { 
        // sample v1 and v2 
        mpfr_urandomb(v1, gst);
        mpfr_urandomb(v2, gst);
        mpfr_mul_ui(v1, v1, 2, MPFR_RNDZ);
        mpfr_sub_ui(v1, v1, 1, MPFR_RNDZ); 
        mpfr_mul_ui(v2, v2, 2, MPFR_RNDZ);
        mpfr_sub_ui(v2, v2, 1, MPFR_RNDZ); 
        // compute alpha
        mpfr_sqr(alpha, v1,  MPFR_RNDZ); 
        mpfr_sqr(a1, v2,  MPFR_RNDZ); 
        mpfr_add(alpha, alpha, a1, MPFR_RNDZ); 
        gen++;
        // rejection check
    } while (mpfr_cmp_ui(alpha, 1) >= 0 || mpfr_cmp_ui(alpha, 0) == 0); 
    // with the accepted pair (v1,v2) compute y1, y2
    mpfr_log(t, alpha, MPFR_RNDZ); 
    mpfr_mul_si(a1, t, -2, MPFR_RNDZ);
    mpfr_div(a1, a1, alpha, MPFR_RNDZ); 
    mpfr_sqrt(t, a1, MPFR_RNDZ); 
    mpfr_mul(y1, v1, t, MPFR_RNDZ); 
    mpfr_mul(y2, v2, t, MPFR_RNDZ);
    // clear variables 
    mpfr_clears(v1,v2,alpha,a1, t, (mpfr_ptr) NULL); 
    // return the number of generations for an acceptance
    return gen; 
}
    
/** 
 * Many samples with the Central Limit Theorem Approach
 */ 
void cltgen(int stor, int terms, int samp, int seed, FILE *outp)
{
    mpfr_t ut, res, c1,c2; 
    mpfr_inits2( stor, ut, res, c1, c2, (mpfr_ptr)NULL); 
    
    // random generator
    gmp_randstate_t rndst; 
    gmp_randinit_default(rndst); 
    gmp_randseed_ui(rndst, seed); 
    
    mpfr_set_ui(c1, (unsigned long) terms, MPFR_RNDZ); 
    mpfr_ui_div(c2, 12L, c1, MPFR_RNDZ); // use first the k constant for c2
    mpfr_sqrt(c2, c2, MPFR_RNDZ); 
    mpfr_div_ui(c1, c1, 2L, MPFR_RNDZ);  
    
    for(int i = 0; i < samp; i++) { 
        mpfr_set_zero(res, 1);
        for (int j=0; j<terms;j++) {
            mpfr_urandomb(ut, rndst);
            mpfr_add(res, res, ut, MPFR_RNDZ); 
        }
        // shift, scale, and print the sample
        mpfr_sub(res, res, c1, MPFR_RNDZ); 
        mpfr_mul(res, res, c2, MPFR_RNDZ); 
        mpfr_out_str(outp, 10, 0, res, MPFR_RNDZ); 
        fprintf(outp, "\n"); 
    }
    // clear vars
    mpfr_clears(ut, res, c1, c2, (mpfr_ptr)NULL); 
    gmp_randclear(rndst);   
}

/**
 * Many Samples with the polar method
 */ 
int pmgen(int it, int st, int samp, int seed, FILE* outp)
{
    assert((samp%2) == 0); // nbr of samples should be pair
    
    int phyp = cordicprec_hyp(it);
    int gen = 0;
    
    // random generator
    gmp_randstate_t rndst; 
    gmp_randinit_default(rndst); 
    gmp_randseed_ui(rndst, seed); 
    // our variables 
    mpfr_t y1, y2; // output
    mpfr_t v1, v2, alpha, a1,  t; // intermediate 
    mpfr_inits2(st, y1, y2, v1, v2, alpha, a1, (mpfr_ptr) NULL); 
    mpfr_init2(t, phyp); 

    for (int i = 0; i < samp/2 ; i++) { 
        do { 
            // sample v1 and v2 
            mpfr_urandomb(v1, rndst);
            mpfr_urandomb(v2, rndst);
            mpfr_mul_ui(v1, v1, 2, MPFR_RNDZ);
            mpfr_sub_ui(v1, v1, 1, MPFR_RNDZ); 
            mpfr_mul_ui(v2, v2, 2, MPFR_RNDZ);
            mpfr_sub_ui(v2, v2, 1, MPFR_RNDZ); 
            // compute alpha
            mpfr_sqr(alpha, v1, MPFR_RNDZ); 
            mpfr_sqr(a1, v2, MPFR_RNDZ); 
            mpfr_add(alpha, alpha, a1, MPFR_RNDZ); 
            gen++;
            // rejection check
        } while (mpfr_cmp_ui(alpha, 1) >= 0 || mpfr_cmp_ui(alpha, 0) == 0); 
        // with the accepted pair (v1,v2) compute y1, y2
        mpfr_log(t, alpha, MPFR_RNDZ); 
        mpfr_mul_si(a1, t, -2, MPFR_RNDZ);
        mpfr_div(a1, a1, alpha, MPFR_RNDZ); 
        mpfr_sqrt(t, a1, MPFR_RNDZ); 
        mpfr_mul(y1, v1, t, MPFR_RNDZ); 
        mpfr_mul(y2, v2, t, MPFR_RNDZ);
        // print? 
        mpfr_out_str(outp, 10, 0, y1, MPFR_RNDZ); 
        fprintf(outp, "\n"); 
        mpfr_out_str(outp, 10, 0, y2, MPFR_RNDZ);
        fprintf(outp, "\n"); 
    }
    // clear variables 
    mpfr_clears(y1, y2, v1,v2,alpha,a1, t, (mpfr_ptr) NULL); 
    gmp_randclear(rndst);
    // return the number of generations for an acceptance
    return gen;
}


/** 
 * Compute gaussian samples using the
 * erfinv with precision @pr 
 * variables have storage @st
 * it uses erfinv function of the boots library
 * and simulates lower precision with 
 **/ 
void erfinv_boost_gen(int pr, int st, int samp, int seed, FILE* outp)
{
    // final erf precision should be smaller than working precision
    assert(pr < st-2); 
    assert(st < 78); // we cannot compute with more precision than 78 bits 
    // random generator
    gmp_randstate_t rndst; 
    gmp_randinit_default(rndst); 
    gmp_randseed_ui(rndst, seed); 
    
    mpfr_t u, y, sqrt2;
    mpfr_inits2(pr, y, (mpfr_ptr)NULL);
    mpfr_inits2(st, sqrt2, u,  (mpfr_ptr)NULL);     
    
    mpfr_sqrt_ui(sqrt2, 2L, MPFR_RNDZ); // sqrt(2) constant 
    for (int i=0; i< samp; i++) { 
        // sample a uniform number in (0,1) and normalize it to (-1, 1)
        do 
            mpfr_urandomb(u, rndst); 
        while (mpfr_zero_p(u));  
        mpfr_mul_ui(u, u, 2L, MPFR_RNDZ); 
        mpfr_sub_ui(u, u, 1L, MPFR_RNDZ); 
        // convert @u to long double y = erfinv(u) with precision @pr
        long double uld = mpfr_get_ld(u, MPFR_RNDZ); 
        mpfr_set_ld(y, boost::math::erf_inv(uld), MPFR_RNDZ); 
        // scale the output value correctly and print 
        mpfr_mul(y, y, sqrt2, MPFR_RNDZ); 
        mpfr_out_str(outp, 10, 0, y, MPFR_RNDZ); 
        fprintf(outp, "\n"); 
    }
    mpfr_clears(u, y, sqrt2, (mpfr_ptr)NULL);
    gmp_randclear(rndst);
} 

void erfinv_sp_gen(int samp, int seed, FILE* outp)
{
    // random generator
    gmp_randstate_t rndst; 
    gmp_randinit_default(rndst); 
    gmp_randseed_ui(rndst, seed); 
    
    int preccent=21; // bit precision in the central region for values no smaller than 3x10^-7
    int prectail = 20; // bit precision in the tail region for values no smaller than 6x10^-7
    int singleprec = 23; // upper bound on the emulation of single precision
    
    mpfr_t u, y, y1, y2, sqrt2;
    mpfr_inits2(preccent, y1, (mpfr_ptr)NULL); 
    mpfr_inits2(prectail, y2, (mpfr_ptr)NULL); 
    mpfr_inits2(singleprec, sqrt2, u, y,  (mpfr_ptr)NULL);     
    mpfr_sqrt_ui(sqrt2, 2L, MPFR_RNDZ); // sqrt(2) constant 
    for (int i=0; i< samp; i++) { 
        // sample a uniform number in (0,1) and normalize it to (-1, 1)
        do 
            mpfr_urandomb(u, rndst); 
        while (mpfr_zero_p(u));  
        mpfr_mul_ui(u, u, 2L, MPFR_RNDZ); 
        mpfr_sub_ui(u, u, 1L, MPFR_RNDZ); 
        // convert @u to long double y = erfinv(u) with precision @pr
        long double uld = mpfr_get_ld(u, MPFR_RNDZ); 
        long double xx = (1L - uld) * (1L + uld);  
        long double w = -1L * logl(xx); 
        if (w < 5.0L) { // in the central region we have precision @preccent
            mpfr_set_ld(y1, boost::math::erf_inv(uld), MPFR_RNDZ); 
            // scale the output value correctly and print 
            mpfr_mul(y, y1, sqrt2, MPFR_RNDZ); 
        } else { // in the tail region, we have precision @prectail
            mpfr_set_ld(y2, boost::math::erf_inv(uld), MPFR_RNDZ); 
            // scale the output value correctly and print 
            mpfr_mul(y, y2, sqrt2, MPFR_RNDZ); 
        }
        mpfr_out_str(outp, 10, 0, y, MPFR_RNDZ);  
        fprintf(outp, "\n"); 
    }
    mpfr_clears(u, y1, y2,y, sqrt2, (mpfr_ptr)NULL);
    gmp_randclear(rndst);
}

/*** 
 *  erfinvspgen(erfp, st, samp, seed, outp): 
 *  Simulates Gopa approach with @erfp bits of precision in the 
 *  computation of the erf function, and the use of @st bits of storage. 
 *  The random number generation is fed with @seed. 
 *  @samp samples are generated and written in @outp file. 
 * 
 *  The approach is to use the erfinv function calibrated to 
 *  abs(erfinv(u) - erfinv(u +/- 2^(1-@erfp)))
 *  where +/- in the second point means that we add or substract 2^(1-@erfp)
 *  to u in the direction of 0. 
 */ 
void erf_gopa_gen(int erfp, int st, int samp, int seed, FILE *outp) 
{ 
    // random generator
    gmp_randstate_t rndst; 
    gmp_randinit_default(rndst); 
    gmp_randseed_ui(rndst, seed); 

    mpfr_t u, y, sqrt2;
    mpfr_inits2(st, sqrt2, u, y, (mpfr_ptr)NULL);     
    mpfr_sqrt_ui(sqrt2, 2L, MPFR_RNDZ); // sqrt(2) constant 
    
    // compute precision step of x
    long double unit = powl(2L, -1L * (long double)erfp);
    
    for (int i=0; i< samp; i++) { 
        // sample a uniform number in (0,1) and normalize it to (-1, 1)
        do 
            mpfr_urandomb(u, rndst); 
        while (mpfr_zero_p(u));  
        mpfr_mul_ui(u, u, 2L, MPFR_RNDZ); 
        mpfr_sub_ui(u, u, 1L, MPFR_RNDZ); 
        // convert @u to long double y = erfinv(u) with precision @pr
        long double uld = mpfr_get_ld(u, MPFR_RNDZ); 
        // compute the point on where we can compare
        long double res = boost::math::erf_inv(uld); 
        // compute a second point to measure the precision of our approx
        long double p1 = boost::math::erf_inv(uld + unit * ((uld<0) ? 1L : -1L));
        // get the precision of the erfinv function 
        long double dif = fabsl(res - p1); 
        // we take into account integer bits to round as MPFR library
        // does not differentiate fractional from integer bits when
        // rounding
        int intbits = ((int) floorl(log2l(fabsl(res)))) + 1; 
        if (intbits < 0) 
            intbits = 0;   
        int fracbits = (int) floorl(log2l(1L/dif));  
        int prec = intbits + fracbits;  
        //~ fprintf(stderr, "fabsl(res - p1)=%Lf\n",  dif);
        //~ fprintf(stderr, "prec=%d\n", prec); 
        // compute the output with the apropriate precision 
        
        mpfr_set_ld(y, res, MPFR_RNDZ); 
        mpfr_prec_round(y, prec, MPFR_RNDZ); 
        mpfr_mul(y, y, sqrt2, MPFR_RNDZ); 
        mpfr_out_str(outp, 10, 0, y, MPFR_RNDZ);  
        fprintf(outp, "\n"); 
    }
    mpfr_clears(u, y, sqrt2, (mpfr_ptr)NULL);
    gmp_randclear(rndst);
}
    

int main(int argc, char *argv[])
{   
    int erfp, prec, storage, samp, seed, clt_terms;  
        if (argc > 1) { 
    switch(*argv[1]){
        case 'b':
        case 'p': 
        case 'e':
        case 'c':
        case 'g': 
            erfp = clt_terms = prec = atoi(argv[2]);  
            storage=atoi(argv[3]); 
            samp=atoi(argv[4]); 
            seed=atoi(argv[5]); 
            switch(*argv[1]){ 
                case 'b':
                    bmgen(prec, storage, samp, seed, stdout);
                    break;
                case 'p': 
                    pmgen(prec, storage, samp, seed, stdout); 
                    break; 
                case 'e':
                    erfinv_boost_gen(prec, storage,samp, seed, stdout); 
                    break; 
                case 'c':
                    cltgen(storage, clt_terms, samp, seed, stdout); 
                    break;
                case 'g':
                    erf_gopa_gen(erfp, storage, samp, seed, stdout);
                    break; 
                default:
                    break;
            }
            break; 
        case 's':
            samp=atoi(argv[2]); 
            seed=atoi(argv[3]); 
            erfinv_sp_gen(samp, seed, stdout);
            break; 
        default:
            printf("./simu type precision/clt_terms storage n_samples\n");  
            break; 
    } 
        } else {
            printf("./simu type precision/clt_terms storage n_samples\n");  
        }
    return 0;  
}




