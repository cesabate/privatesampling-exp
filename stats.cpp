// Compile with 
// g++-10 -std=gnu++2a stats.cpp -o stats -O3 -ffast-math 

#include <iostream>
#include <fstream>
#include <math.h>
#include <numbers>
#include <random>
#include <vector>
#include <algorithm>
#include <boost/math/special_functions/erf.hpp>

typedef long double real;

inline real gauss_func(real x) {
  return (1/sqrt(2*std::numbers::pi_v<real>))*exp(-x*x/2);
}

inline real cumgauss_inv(real x) {
  return boost::math::erf_inv(2*x-1) * std::sqrt(2);
}

int main(int argc, char *argv[]) {
  if(argc>2) {
    std::cerr << "more then 1 argument\n";
    exit(3);
  } else if(argc==2) {
    //~ std::ifstream f;as
    //~ f.open(argv[1]);
    //~ std::cout << argv[1] << " : ";
    //~ if(!f.is_open()) {
      //~ std::cerr << "can't open file " << argv[0] << "\n";
      //~ exit(3);
    //~ } else {      
      real x;
      std::vector<real> xvect;
      long unsigned n=0;
      real ll = 0;
      while(!std::cin.eof()) {
    std::cin >> x;
    xvect.push_back(x);
    n++; 
    real p_x = gauss_func(x);
    ll += log(p_x);
      }
      
      ll /= n;
      
      //std::cout << "last x: "<<x<<"\n";
      std::cout << "n=" << n;
      std::cout << "\tll=" << ll;
      
      std::sort(xvect.begin(),xvect.end());
      real cum_mse = 0.0;
      
      for(unsigned i=0;i<n;i++) {
    real diff = xvect[i] - cumgauss_inv((i+1)*((real)1.0)/(n+1));
    cum_mse += diff*diff;
      }
      cum_mse /= n;
  
      std::cout << "\tcmse= " << cum_mse << "\n";
    //~ }
  } else { // argc == 1;
    std::cout << "Perfect:";
    const unsigned n=10000000;
    std::cout << "\tn="<<n;
    
    std::default_random_engine rndg;
    std::normal_distribution<real> gauss_dist;
    
    const unsigned perfect_ss = 10;
    real perfect_ll[perfect_ss];
    for(unsigned i=0;i<perfect_ss; i++)
      perfect_ll[i] = 0.0;
    
    real perfcum_mse[perfect_ss];
    for(unsigned i=0; i<perfect_ss; i++) {
      std::vector<real> perfxvect;
      for(unsigned j=0; j<n; j++) {
    real perfx = gauss_dist(rndg);
    real p_perfx = gauss_func(perfx);
    perfxvect.push_back(perfx);
    perfect_ll[i] += log(p_perfx);
      }
      perfect_ll[i] /= n;
      std::sort(perfxvect.begin(),perfxvect.end());
      perfcum_mse[i] = 0.0;
      for(unsigned j=0;j<n;j++) {
    real diff = perfxvect[j] - cumgauss_inv((j+1)*((real)1.0)/(n+1));
    perfcum_mse[i] += diff*diff;
      }
      perfcum_mse[i] /= n;
    }
    
    real perfect_ll_avg = 0.0;
    real perfect_ll_var = 0.0;
    real perfcum_mse_avg = 0.0;
    real perfcum_mse_var = 0.0;
    for(unsigned i=0;i<perfect_ss;i++) {
      perfect_ll_avg += perfect_ll[i];
      perfcum_mse_avg += perfcum_mse[i];
    }
    perfect_ll_avg /= perfect_ss;
    perfcum_mse_avg /= perfect_ss;
    for(unsigned i=0;i<perfect_ss;i++) {
      real diff = perfect_ll_avg - perfect_ll[i];
      perfect_ll_var += diff*diff;
      diff = perfcum_mse_avg - perfcum_mse[i];
      perfcum_mse_var += diff*diff;
    } 
    perfect_ll_var /= perfect_ss;
    perfcum_mse_var /= perfect_ss;
    
    std::cout << "\tll = "<< perfect_ll_avg << " +- " << std::sqrt(perfect_ll_var);
    std::cout << "\tcmse:"<< perfcum_mse_avg << " +- " << std::sqrt(perfcum_mse_var) <<"\n";
    
    return 0;
  }
}
