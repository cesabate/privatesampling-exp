import cost
import sys

assert(len(sys.argv) > 1) 

com = int(sys.argv[1])

if com == 1: 
    cost.plot_GEl()
elif com == 2: 
    cost.plot_GExp(0)
elif com == 3: 
    cost.plot_GExp(1) 
elif com == 4:
    cost.plot_GExp_prov_zoom()
elif com == 5:
    cost.plot_GExp_verif_zoom() 
elif com == 6:
    cost.plot_GEl_Classic_PolM()
elif com == 7:
    cost.plot_GEl_Classic_CLT()
else: 
    print('Unrecognized parameter.')
