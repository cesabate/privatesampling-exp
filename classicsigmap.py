import math as m

# one cordic unit
def cord1_cmp(it, st):
    return 5 * (it **2) + 24 * it  + 10 * m.log2(it) 
    
def cord1_com(it, st):
    return  9 * (it **2) + 15 * it + 3 * m.log2(it) 
    
def cord2_cmp(it, st):
    return 5 * (it **2) + 14 * it  + 10 * st
    
def cord2_com(it, st):
    return 9 * (it **2) + 12 * it + 3 * st
    
    ## Trascendental functions BM + generalized domain of CORDIC
         
    
def trig_cmp(it, st):
    return cord1_cmp(it, st) + 57
    
def trig_com(it, st): 
    return cord1_com(it, st) + 23
    
def ln_cmp(it, st):
    return cord2_cmp(it, st) + 5  #* st + 28 * m.log2(gz)
    
def ln_com(it, st):
    return cord2_com(it, st) + 1.5 * st # + 14 * m.log2(gz)
      
def sqrt_cmp(it, st):
    return cord2_cmp(it, st) + 17 * st # + 87 * m.log2(gz) + 10*gz + 18  

def sqrt_com(it, st):
    return cord2_com(it, st) + 7.5 * st # + 32 * m.log2(gz) + 3*gz + 7
    
# below are defined the constants for range proofs
# the constant is 10 for computation and 6 for communication
rng_com = 6 
rng_cmp = 10

# ~ display('samples_pm_20_25_10M', 'CORDIC+PM 25 it.', -4, 4)
# computes the cost of a zkp
# it are the number of iterations of cordic
# st are the bits of precision
# gz is the group bit size 
# methods return a tuple (communication cost, computation cost) 
def cbm_cost(it, st):
    cmp_tot =  trig_cmp(it, st) + ln_cmp(it, st) + sqrt_cmp(it, st) + 3 * rng_cmp * st 
    com_tot =  trig_com(it, st) + ln_com(it, st) + sqrt_com(it, st) + 3 * rng_com * st 
    # two nubmers are generated, so divide by 2 if needed 
    return  com_tot, cmp_tot
    
def cpm_cost(it, st):
    comp_tot = ln_cmp(it, st) + sqrt_cmp(it, st) + 8 * rng_cmp * st
    com_tot = ln_com(it, st) + sqrt_com(it, st) + 8 * rng_com * st
    # two nubmers are generated, so divide by 2 if needed
    return  com_tot, comp_tot

# generation of a random number     
def clt_cost(ut, st): 
    return  rng_com * st * ut, rng_cmp * st * ut
