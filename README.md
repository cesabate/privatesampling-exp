# Description

This is a tool to reproduce experiments and generate the plots of Figures 1, 2 and 3 of Section 9 of the article. 


# Required Packages 

The code was tested in Ubuntu 20.04.05. The packages listed below are required before the setup and execution of experiments. We specify the versions of them that were used for testing.  

- Python v. 3.8.10
- GNU C++ Compiler v. 10.3.0 (g++-10)

- GMP C/C++ library v. 6.2.0 
- MPFR C library v. 4.0.2
- BOOST C++ library v. 1.71 

- Python 3 Pip v. 20.0.2
- NumPy v. 1.23.5
- matplotlib v. 3.6.2

# Setup

Run
```
bash setup.sh
```
to compile C++ code. 


# Documentation 

Our experiments compare sampling techniques to generate Gaussian samples in zero knowledge presented in the article. For each technique, we measure the statistical quality of the sample against the cryptographic communication and computation cost for different precision parameters. 

The source code is written in C++ and Python. Below, we describe each source file. 

- **simu.cpp**: Provides functions that, given a sampling technique and its precision parameters, output samples of the Gaussian distribution.  

- **stats.cpp**: Measures the statistical quality of a set of samples with respect to an ideal Gaussian. The statistical quality is measured using the Mean Square Error (MSE).

- **cost.py**: Provides functions that, given a sampling technique and its precision parameters, estimate the cryptographic cost of generating one sample when the technique is implemented using compressed Sigma-protocols. The estimated cryptographic cost includes the number of group exponentiations made by the prover and verifier, and the number of cryptographic group elements in the proof.

- **classicsigmap.py**: Provides functions that, given a sampling technique and its precision parameters, estimate the communication cost of generating one sample when the technique is implemented using classic Sigma-protocols. The communication cost is measured by counting the number of cryptographic group elements in the proof.

- **plot.py**: Generates a plot given a plot number. Each plot  includes a set of curves, one per sampling technique. Each point of a curve represents different precision parameters. For each point, performance is evaluated by (1) generating samples using *simu.cpp*, (2) evaluating them using *stats.cpp* and (3) estimating the cryptographic cost required to generate one sample using either *cost.py* or *classicsigmap.py* (depending on the type of plot). Plots are described in more detail below.   


 
# Usage 

Plots can be generated with

```
python3 plot.py [number of plot] 
```

- **Plot n. 1**: Generates Figure 1 of the article. Shows the required cryptographic group elements for one sample against the MSE for BM, PolM, InvM and CLT approaches.

- **Plot n. 2**: Generates Figure 2a (left) of the article. Shows the required cryptographic group exponentiations for the prover for one sample against the MSE for BM, PolM, InvM and CLT approaches. 

- **Plot n. 3**: Generates Figure 2b (left) of the article. Shows the required cryptographic group exponentiations for the verifier for one sample against the MSE for BM, PolM, InvM and CLT approaches. 

- **Plot n. 4**: Generates Figure 2a (right) of the article. This plot is a zoom in logarithmic scale of Plot n. 2, showing BM, PolM and CLT in more detail. 

- **Plot n. 5**: Generates Figure 2b (right) of the article. This plot is a zoom in logarithmic scale of Plot n. 3, showing BM, PolM and CLT in more detail. 

- **Plot n. 6**: Generates Figure 3 (left) of the article. Shows the required cryptographic group elements for one sample against the MSE using compressed and classic Sigma-protocols for the PolM technique.

- **Plot n. 7**: Generates Figure 3 (right) of the article.  Shows the required cryptographic group elements for one sample against the MSE using compressed and classic Sigma-protocols for the CLT technique. 



